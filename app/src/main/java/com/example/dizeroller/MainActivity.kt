package com.example.dizeroller
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import com.google.android.material.snackbar.Snackbar
import java.util.*
import kotlin.random.Random
// The key ':' in a class header means it extends of the subsequent class.
class MainActivity : AppCompatActivity()
{
    private lateinit var rollButton : Button
    private lateinit var resetButton : Button
    private lateinit var leftDice : ImageView
    private lateinit var rightDice : ImageView
    private lateinit var sixthDice : ImageView
    private var dices = listOf(
        R.drawable.dice_1,
        R.drawable.dice_2,
        R.drawable.dice_3,
        R.drawable.dice_4,
        R.drawable.dice_5,
        R.drawable.dice_6)
    /**
     * Sets a listener on a single ImageView that changes the value of a dice when it's pressed,
     * and shows a snack bar if both are the sixth dice.
     *
     * @param dice The ImageView of the dice that will be interacted with, individually.
     */
    private fun setSingleDiceListener( dice : ImageView )
    {
        dice.setOnClickListener {
            interact(dice)
            showSnackBarIfBothSix(dice)
        }
    }
    /**
     * Sets a listener on each dice individually.
     */
    private fun setSingleDiceListener()
    {
        setSingleDiceListener(leftDice)
        setSingleDiceListener(rightDice)
    }
    /**
     * Sets a listener on the resetButton that changes the drawable to be the default one on both dices.
     */
    private fun setResetButtonListener()
    {
        resetButton.setOnClickListener {
            leftDice.setBackgroundResource( R.drawable.empty_dice )
            rightDice.setBackgroundResource( R.drawable.empty_dice )
        }
    }
    /**
     * Initializes an animation in this Activity from the drawable "shake" that shakes a dice, and executes it.
     *
     * @param dice The ImageView of the dice from which its shake animation will be executed.
     */
    private fun shake( dice : ImageView )
    {
        val shake = AnimationUtils.loadAnimation(this, R.anim.shake)
        dice.startAnimation(shake)
    }
    /**
     * Initializes a random value from 0 to 6 (excluded), then the dice background resource is changed
     * to the one in the "dices" Array which matches its value with the random value, and shakes the dice.
     *
     * @param dice The ImageView of the dice which will be interacted with.
     */
    private fun interact( dice : ImageView )
    {
        val random = Random.nextInt(0, 6)
        dice.setBackgroundResource( dices[ random ] )
        shake(dice)
    }
    /**
     * Returns true if the dice value is six, false otherwise.
     *
     * @param dice The ImageView of the dice which will be compared its value with.
     */
    private fun equalsToSix( dice : ImageView ) : Boolean
    {
        return Objects.equals( dice.getBackground().getConstantState(), sixthDice.getBackground().getConstantState() )
    }
    /**
     * If both dices values are six, a snack bar prompting "JACKPOT!" will be shown.
     *
     * @param view The view to find a parent from, it is used to make the snack bar from it.
     */
    private fun showSnackBarIfBothSix( view : View )
    {
        if ( equalsToSix(leftDice) && equalsToSix(rightDice) ) Snackbar.make( view, "JACKPOT!", Snackbar.LENGTH_LONG ).show()
    }
    /**
     * Sets a listener on the rollButton that interacts with both dices and shows a snack bar if both are the sixth dice.
     */
    private fun setRollButtonListener()
    {
        rollButton.setOnClickListener {
            interact(leftDice)
            interact(rightDice)
            showSnackBarIfBothSix(rollButton)
        }
    }
    /**
     * Sets a listener for the rollButton, resetButton and each dice.
     */
    private fun setListeners()
    {
        setRollButtonListener()
        setResetButtonListener()
        setSingleDiceListener()
    }
    /**
     * Initializes the rollButton, resetButton, leftDice, rightDice, sixthDice and
     * sets the sixthDice background resource to match it with the sixth dice value.
     */
    private fun initializeLateInitVariables()
    {
        rollButton = findViewById( R.id.button )
        resetButton = findViewById( R.id.resetButton )
        leftDice = findViewById( R.id.leftDiceView )
        rightDice = findViewById( R.id.rightDiceView )
        sixthDice = findViewById( R.id.sixthDiceView )
        sixthDice.setBackgroundResource( dices[5] )
    }
    /**
     * The core of execution, initializes late init variables and sets all the listeners.
     */
    override fun onCreate( savedInstanceState : Bundle? )
    {
        super.onCreate( savedInstanceState )
        setContentView( R.layout.activity_main )
        initializeLateInitVariables()
        setListeners()
    }
}